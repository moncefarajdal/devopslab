import json
import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_title_api(client):
    response = client.get('/api/title')
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 200
    assert data['title'] == 'Gitlab CI & Azure Lab'

def test_echo_api(client):
    input_text = "Hello, World!"
    response = client.post('/api/echo', data={'text': input_text})
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 200
    assert data['title'] == 'Input Text'
    assert data['text'] == input_text

def test_calculate_api(client):
    num1 = 5
    num2 = 2
    operator = 'add'
    response = client.post('/api/calculate', data={'num1': num1, 'num2': num2, 'operator': operator})
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 200
    assert data['title'] == 'Calculation Result'
    assert data['result'] == num1 + num2

    # Test division by zero
    num2 = 0
    operator = 'divide'
    response = client.post('/api/calculate', data={'num1': num1, 'num2': num2, 'operator': operator})
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 200
    assert 'error' in data

    # Test invalid operator
    operator = 'invalid'
    response = client.post('/api/calculate', data={'num1': num1, 'num2': num2, 'operator': operator})
    data = json.loads(response.get_data(as_text=True))
    assert response.status_code == 200
    assert 'error' in data

if __name__ == '__main__':
    pytest.main()
