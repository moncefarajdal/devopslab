from flask import Flask, render_template, request, jsonify

app = Flask(__name__)

# Initialize a global variable to store the input text
input_text = ''

# API 1: Returns the title
@app.route('/api/title', methods=['GET'])
def get_title():
    return jsonify({'title': 'Gitlab CI & Azure Lab'})

# API 2: Takes a text input and sets the global variable
@app.route('/api/echo', methods=['POST'])
def echo_text():
    global input_text
    input_text = request.form.get('text')
    return jsonify({'title': 'Input Text', 'text': input_text})

# API 3: Takes two numbers and an operator, calculates the result
@app.route('/api/calculate', methods=['POST'])
def calculate():
    num1 = float(request.form.get('num1'))
    num2 = float(request.form.get('num2'))
    operator = request.form.get('operator')

    if operator == 'add':
        result = num1 + num2
    elif operator == 'subtract':
        result = num1 - num2
    elif operator == 'multiply':
        result = num1 * num2
    elif operator == 'divide':
        if num2 == 0:
            return jsonify({'error': 'Division by zero!'})
        result = num1 / num2
    else:
        return jsonify({'error': 'Invalid operator!'})

    return jsonify({'title': 'Calculation Result', 'result': result})

# Render the main HTML page
@app.route('/')
def index():
    return render_template('index.html', input_text=input_text)

if __name__ == '__main__':
    app.run(debug=True)
